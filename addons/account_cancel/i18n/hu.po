# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* account_cancel
# 
# Translators:
# krnkris, 2019
# gezza <geza.nagy@oregional.hu>, 2019
# Ákos Nagy <akos.nagy@oregional.hu>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-12 11:32+0000\n"
"PO-Revision-Date: 2019-08-26 09:08+0000\n"
"Last-Translator: Ákos Nagy <akos.nagy@oregional.hu>, 2019\n"
"Language-Team: Hungarian (https://www.transifex.com/odoo/teams/41243/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: account_cancel
#: model:ir.model,name:account_cancel.model_account_bank_statement
msgid "Bank Statement"
msgstr "Bankkivonat"

#. module: account_cancel
#: model_terms:ir.ui.view,arch_db:account_cancel.payment_cancel_form_inherit
msgid "Cancel"
msgstr "Mégse"

#. module: account_cancel
#: model_terms:ir.ui.view,arch_db:account_cancel.view_move_form_inherit_account_cancel
msgid "Cancel Entry"
msgstr "Tétel visszavonása"

#. module: account_cancel
#: model_terms:ir.ui.view,arch_db:account_cancel.view_move_form_inherit_account_cancel
msgid "Reset to Draft"
msgstr "Visszaállítás piszkozat állapotba"

#. module: account_cancel
#: model_terms:ir.ui.view,arch_db:account_cancel.bank_statement_draft_form_inherit
msgid "Reset to New"
msgstr "Visszaállítás új változatra"

#. module: account_cancel
#: model_terms:ir.ui.view,arch_db:account_cancel.bank_statement_cancel_form_inherit
msgid "Revert reconciliation"
msgstr "Egyeztetés visszafordítása"
